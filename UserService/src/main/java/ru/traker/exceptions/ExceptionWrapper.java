package ru.traker.exceptions;

public interface ExceptionWrapper {
    String getCode();

    String getMessage();
}