package ru.traker.exceptions;

public class UserException extends RuntimeException implements ExceptionWrapper {
    private final String code;

    public UserException(String message, String code) {
        super(message);
        this.code = code;
    }

    @Override
    public String getCode() {
        return code;
    }
}