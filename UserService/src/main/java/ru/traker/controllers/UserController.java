package ru.traker.controllers;

import com.fasterxml.jackson.core.JsonProcessingException;
import jakarta.validation.Valid;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import ru.traker.dto.UserRegisterRequestDto;
import ru.traker.dto.UserRegisterResponseDto;
import ru.traker.services.UserService;

@RestController
@RequestMapping("/api/v1/user")
public class UserController {

    public UserController(UserService userService) {
        this.userService = userService;
    }

    UserService userService;



    @PostMapping("/register")
    public UserRegisterResponseDto registerUser(@Valid @RequestBody UserRegisterRequestDto userRegisterRequestDto){
        userService.saveUser(userRegisterRequestDto);
        return UserRegisterResponseDto.builder().message("Пользователь зарегистрирован").build();
    }

    @GetMapping("/getAll")
    public ResponseEntity<?> getAllUsers(){
        return userService.getAllUsers();
    }

    @PostMapping("/delete")
    public ResponseEntity<?> deleteUserById(@RequestParam Long id){
        return userService.deleteUser(id);
    }


}
