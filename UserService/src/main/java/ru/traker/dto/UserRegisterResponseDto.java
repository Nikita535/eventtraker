package ru.traker.dto;

import lombok.*;

@Builder
@Getter
@Setter
@NoArgsConstructor
public class UserRegisterResponseDto {
    private String message;

    public UserRegisterResponseDto(String message) {
        super();
        this.message = message;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
