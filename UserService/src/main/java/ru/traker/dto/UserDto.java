package ru.traker.dto;

import jakarta.validation.constraints.Email;
import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.Size;
import lombok.*;
import org.springframework.stereotype.Component;

@Getter
@Setter
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class UserDto {
    @NotNull(message = "Имя должно быть введено.")
    @Size(min = 4, message = "Длина имени должна быть минимум из четырёх символов.")
    private String username;

    @NotNull(message = "Пароль должен быть введён.")
    @Size(min = 5, message = "Пароль должен быть минимум из 5 символов.")
    private String password;
    private Object authorities;

    @NotNull(message = "Почта должна быть введена.")
    @Size(min = 1, message = "Почта должна быть введена.")
    @Email(message = "Почта не подходит под формат ввода.")
    private String email;

}