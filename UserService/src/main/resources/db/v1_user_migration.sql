CREATE TABLE if not exists users (
                       id SERIAL PRIMARY KEY,
                       user_name VARCHAR(255) NOT NULL,
                       password VARCHAR(255) NOT NULL,
                       email VARCHAR(255) UNIQUE NOT NULL,
                       active BOOLEAN NOT NULL
);

-- Создание таблицы "user_role"
CREATE TABLE if not exists user_role (
                           user_id BIGINT NOT NULL,
                           authority VARCHAR(50) NOT NULL,
                           PRIMARY KEY (user_id, authority),
                           FOREIGN KEY (user_id) REFERENCES users (id) ON DELETE CASCADE
);


INSERT INTO users VALUES (1,'Nikita',
                          '$2a$12$PJR1j1PUO7YRtiuf2XKU7OowYgYBdh60pfY1ZdkzAcZVJPugNdow.',
                          'nzhigulevskiy@bk.ru',
                          true);
INSERT INTO user_role VALUES (1,'USER');